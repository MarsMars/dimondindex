#include "pch.h"
#include "StringTool.h"
#include <stack>

StringTool::StringTool(char text[]) : text(text)
{
}

void StringTool::invert()
{
	stack<char> chars;
	const char* tp = text;
	char *buffer, *ip = NULL;
	const int size = this->count() + 1;

	buffer = new char[size];
	ip = buffer;
	fill(ip, ip + size, 0);

	tp += this->count();

	for (int i = 0; i < this->count(); i++)
		*ip++ = *--tp;

	strcpy(text, buffer);

	delete [] buffer;
}

int StringTool::count()
{
	int count = 0;
	char* p = text;

	while (*p != '\0') {
		count++;
		p++;
	}

	return count;
}

void StringTool::lowercase()
{
}

void StringTool::asci()
{
}
