#include "pch.h"


BinaryTree::BinaryTree(int value)
{
	root = new BinaryTreeNode(value);
}

void BinaryTree::AddNode(int value) {
	BinaryTreeNode** leaf = &root;


	while (*leaf != NULL)
		leaf = ((*leaf)->value >= value) ? &(*leaf)->left : &(*leaf)->right;

	*leaf = new BinaryTreeNode(value);
}

int BinaryTree::CountValue(int value)
{
	return 0;
}

int BinaryTree::MaxDepth()
{
	return 0;
}


int BinaryTreeRecursive::MaxDepth() {
	return MaxDepthWithParameter(root)+1;
}

int BinaryTreeRecursive::MaxDepthWithParameter(BinaryTreeNode* node) {
	if (node == NULL)
		return -1;
	else {
		int lDepth = MaxDepthWithParameter(node->left);
		int rDepth = MaxDepthWithParameter(node->right);

		if (lDepth > rDepth)
			return(lDepth + 1);
		else return(rDepth + 1);
	}
}

BinaryTreeRecursive::BinaryTreeRecursive(int value) : BinaryTree(value)
{
}

int BinaryTreeRecursive::CountValue(int value)
{
	return CountValueRec(value);
}

int BinaryTreeRecursive::CountValueRec(int value, BinaryTreeNode** leaf, int if_first, int counter) {
	if (if_first == 1)
		leaf = &root;
	if (*leaf == NULL) {
		return counter;
	}

	if ((*leaf)->value == value)
		counter++;

	if ((*leaf)->value >= value)
		CountValueRec(value, &(*leaf)->left, 0, counter);
	else
		CountValueRec(value, &(*leaf)->right, 0, counter);
}




int BinaryTreeIterative::MaxDepth() {
	int depth = 0;

	stack<BinaryTreeNode*> current_level_nodes;
	stack<BinaryTreeNode*> next_level_nodes;
	current_level_nodes.push(root);

	while (!current_level_nodes.empty()) {
		BinaryTreeNode* node = current_level_nodes.top();

		if (!node->isLastLeaf()) {
			if (node->left != NULL)
				next_level_nodes.push(node->left);
			if (node->right != NULL)
				next_level_nodes.push(node->right);

		}
		current_level_nodes.pop();
		if (current_level_nodes.empty()) {
			swap(current_level_nodes, next_level_nodes);
			depth++;
		}
	}
	return depth;
}



BinaryTreeIterative::BinaryTreeIterative(int value) : BinaryTree(value)
{
}

int BinaryTreeIterative::CountValue(int value) {
	BinaryTreeNode** leaf = &root;
	int counter = 0;

	while (*leaf != NULL) {
		if ((*leaf)->value == value)
			counter++;
		leaf = ((*leaf)->value >= value) ? &(*leaf)->left : &(*leaf)->right;
	}
	return counter;
}

bool BinaryTreeNode::isLastLeaf()
{
	return left == NULL && right == NULL;
}
