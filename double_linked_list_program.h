#pragma once
#include <iostream>
#include <string>
#include <stdlib.h>

using namespace std;



class DoubleLinkedListElement {
public:
	DoubleLinkedListElement(int value) : value(value){};
	DoubleLinkedListElement* next = NULL;
	DoubleLinkedListElement* previous = NULL;
	int value = 0;
};

class DoubleLinkedListIterator {
public:
	DoubleLinkedListElement* p;
	DoubleLinkedListIterator(DoubleLinkedListElement* p) : p(p) {}
	bool operator!=(DoubleLinkedListIterator rhs) { return p != rhs.p; }
	DoubleLinkedListElement& operator*() { return *p; }
	void operator++() { p = p->next; }
};

class DoubleLinkedList {
private:
	DoubleLinkedListElement* head = NULL;
	DoubleLinkedListElement* tail = NULL;
public:
	void addhead(int value);
	void setFirstElement(DoubleLinkedListElement* newElement);
	void addtail(int value);
	int remove(int index);
	string listAllElements();
	int find(int value,int places=-1);
	int size() const;
	int getelement(int index) const;
	void clear();
	bool isEmpty() const {
		return (head == NULL && tail == NULL);
	};
	DoubleLinkedList operator+(const DoubleLinkedList& b) const; 
	DoubleLinkedList& operator=(const DoubleLinkedList& b);
	
	DoubleLinkedListIterator begin() { return DoubleLinkedListIterator(head); }
	DoubleLinkedListIterator end() { return DoubleLinkedListIterator(NULL); }
};

