#include "pch.h"


using namespace std;

TEST(Hex, ShouldPrintNumberInHexFormat) {
	// GIVEN-WHEN-THEN
	ASSERT_STREQ(Hex('A').str().c_str(), "0x41");
}