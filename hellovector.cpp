#include <vector>
#include <string>

using namespace std;

vector<string> get_msg(void){
    vector<string> msg {"Hello", "C++", "World", "from", "VS Code", "and the C++ extension!"};
    return msg;
}
