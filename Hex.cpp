#include "pch.h"

Hex::Hex(unsigned char number) : number(number)
{
}

string Hex::str()
{
	ostringstream stream;

	stream << "0x" << setfill('0') << setw(2) << right << hex << (int)number;

	return stream.str();
}
