#include "pch.h"


using namespace std;

TEST(StringTool, ShouldInvertGivenText) {
	// GIVEN
	char text[] = "Tymek";
	StringTool st = StringTool(text);

	// WHEN
	st.invert();

	// THEN
	ASSERT_STREQ(text, "kemyT");
}

TEST(StringTool, ShouldChangeToLowerCase) {
	// GIVEN
	char text[] = "Tymek";
	StringTool st = StringTool(text);

	// WHEN
	st.lowercase();

	// THEN
	ASSERT_STREQ(text, "tymek");
}

TEST(StringTool, ShouldChangeToAsci) {
	// GIVEN
	char text[] = "Tymek";
	StringTool st = StringTool(text);

	// WHEN
	st.asci();

	// THEN
	ASSERT_STREQ(text, "54796D656B");
}


TEST(StringTool, ShouldCountNumberOfCharactersInGivenText) {
	// GIVEN
	char text[] = "Tymek";
	StringTool st = StringTool(text);

	// WHEN - THEN
	ASSERT_EQ(st.count(), 5);
}
