#include "pch.h"

using namespace std;

void DoubleLinkedList::setFirstElement(DoubleLinkedListElement* newElement)
{
	head = newElement;
	tail = newElement;
}

void DoubleLinkedList::addhead(int value) {
	DoubleLinkedListElement* newElement = new DoubleLinkedListElement(value);

	if (isEmpty()) {
		setFirstElement(newElement);
	}
	else {
		newElement->next = head;
		head->previous = newElement;
		head = newElement;
	}
}



void DoubleLinkedList::addtail(int value) {
	DoubleLinkedListElement* newElement = new DoubleLinkedListElement(value);

	if (isEmpty()) {
		setFirstElement(newElement);
	}
	else {
		newElement->previous = tail;
		tail->next = newElement;
		tail = newElement;
	}
}

int DoubleLinkedList::remove(int index) {
	int counter = 0;
	int value;
	DoubleLinkedListElement* next = head;
	DoubleLinkedListElement* last = NULL;
	DoubleLinkedListElement* afternext = NULL;


	if (index<0 || index >= this->size()) {
		return -999999;
	}

	if (this->size() == 1) {
		head = NULL;
		tail = NULL;
		value = next->value;
		delete next;
		return value;
	}

	if (index == 0) {
		value = next->value;
		head = next->next;
		head->previous = NULL;
		delete next;
		return value;
	}

	while (counter != index) {
		last = next;
		next = next->next;
		counter++;
	}
	if (next->next == NULL) {
		last->next = NULL;
		tail = last;
		value = next->value;
		delete next;
		return value;
	}
	afternext = next->next;
	afternext->previous = last;
	last->next = afternext;
	value = next->value;
	delete next;
	return value;
	
}

string DoubleLinkedList::listAllElements() {
	string elements = "";
	DoubleLinkedListElement* next = head;
	while (next != NULL) {
		elements += to_string(next->value);
		next = next->next;
	}
	return elements;
}

int DoubleLinkedList::find(int valuetofind, int places) {
	DoubleLinkedListElement* next = head;
	int counter = 0;
	int pointtostart;
	if (places == -1)
		pointtostart = 0;
	else
		pointtostart = places + 1;
	

	while (next != NULL) {
		if (next->value == valuetofind && counter>=pointtostart) {
			return counter;
		}
		next = next->next;
		counter++;
	}
	return -99999999;
}


int DoubleLinkedList::size() const {
	int counter = 1;
	DoubleLinkedListElement* next = head;
	if (isEmpty()) {
		return 0;
	}
	while (next->next != NULL) {
		counter++;
		next = next->next;
	}

	return counter;
}

int DoubleLinkedList::getelement(int index) const {
	int counter = 0;
	DoubleLinkedListElement* next = head;
	if (index == 0)
		return next->value;

	while (counter != index)
	{
		next = next->next;
		counter++;
	}
	return next->value;
}

void DoubleLinkedList::clear()
{
	while (this->size() != 0) {
		this->remove(0);
	}
}

DoubleLinkedList DoubleLinkedList::operator+(const DoubleLinkedList& b) const {
	DoubleLinkedList c;

	c = *this;

	for (int j = 0;j < b.size();j++) {
		c.addtail(b.getelement(j));
	}
	return c;
}

DoubleLinkedList& DoubleLinkedList::operator=(const DoubleLinkedList& b) {
	this->clear();

	for (int i = 0;i < b.size();i++) {
		this->addtail(b.getelement(i));
	}

	return *this;
}
