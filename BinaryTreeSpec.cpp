#include "pch.h"


using namespace std;

TEST(BinaryTree, ShouldCreateRootElementWithGivenValue) {
	//GIVEN
	int root_value = 0;

	//WHEN
	BinaryTree bt = BinaryTree(root_value);

	//THEN
	ASSERT_EQ(root_value, bt.root->value);
}

TEST(BinaryTree, ShouldAddToTheLeftOfTheRootWhenGivenValueLessThanRootValue) {
	//GIVEN
	int root_value = 5;
	int new_value = 3;

	BinaryTree bt = BinaryTree(root_value);
	
	//WHEN
	bt.AddNode(new_value);

	//THEN
	ASSERT_EQ(bt.root->left->value, new_value);
}

TEST(BinaryTree, ShouldAddToTheLeftLeftOfTheRootWhenGivenValueLessThanRootValue) {
	//GIVEN
	int root_value = 5;
	int first_value = 3;
	int new_value = 2;

	BinaryTree bt = BinaryTree(root_value);

	//WHEN
	bt.AddNode(first_value);
	bt.AddNode(new_value);
	//THEN
	ASSERT_EQ(bt.root->left->left->value, new_value);
}

TEST(BinaryTree, ShouldAddToTheLeftLeftLeftOfTheRootWhenGivenValueLessThanRootValue) {
	//GIVEN
	int root_value = 5;
	int first_value = 4;
	int second_value = 3;
	int new_value = 2;

	BinaryTree bt = BinaryTree(root_value);

	//WHEN
	bt.AddNode(first_value);
	bt.AddNode(second_value);
	bt.AddNode(new_value);
	//THEN
	ASSERT_EQ(bt.root->left->left->left->value, new_value);
}

TEST(BinaryTree, ShouldAddToTheRightOfTheRootWhenGivenValueMoreThanRootValue) {
	//GIVEN
	int root_value = 5;
	int new_value = 7;

	BinaryTree bt = BinaryTree(root_value);

	//WHEN
	bt.AddNode(new_value);

	//THEN
	ASSERT_EQ(bt.root->right->value, new_value);
}

TEST(BinaryTree, ShouldCreateATreeInARightOrder) {
	//GIVEN
	int root_value = 0;
	int left_value = 0;
	int left_left_value = 0;
	int right_value = 1;
	int right_left_value = 1;

	BinaryTree bt = BinaryTree(root_value);

	//WHEN
	bt.AddNode(left_value);
	bt.AddNode(left_left_value);
	bt.AddNode(right_value);
	bt.AddNode(right_left_value);

	//THEN
	ASSERT_EQ(bt.root->right->left->value, right_left_value);


}

void ShouldCountNumberOfGivenValue(BinaryTree& bt) {
	//GIVEN
	int first_value = 3;
	int second_value = 7;
	int third_value = 8;
	bt.AddNode(first_value);
	bt.AddNode(second_value);
	bt.AddNode(third_value);
	bt.AddNode(second_value);
	//WHEN
	int counter = bt.CountValue(second_value);
	//THEN
	ASSERT_EQ(counter, 2);
}
TEST(BinaryTree, ShouldCountNumberOfGivenValueInteratively) {
	ShouldCountNumberOfGivenValue(BinaryTreeIterative(5));
}


TEST(BinaryTree, ShouldCountNumberOfGivenValueRecursively) {
	ShouldCountNumberOfGivenValue(BinaryTreeRecursive(5));
}


void ShouldCountMaxDepth(BinaryTree& bt) {
	//GIVEN
	int first_value = 3;
	int second_value = 7;
	int third_value = 8;
	bt.AddNode(first_value);
	bt.AddNode(second_value);
	// bt.AddNode(third_value);
	//WHEN
	int number = bt.MaxDepth();
	//THEN
	ASSERT_EQ(number, 2);
}

TEST(BinaryTree, ShouldCountMaxDepthRecursively) {
	ShouldCountMaxDepth(BinaryTreeRecursive(5));
}

TEST(BinaryTree, ShouldCountMaxDepthIteratively) {
	ShouldCountMaxDepth(BinaryTreeIterative(5));
}

TEST(BinaryTree, ShouldGetDepthOfOneWhenOnlyRootInTree) {
	// GIVEN
	int first_value = 1;
	BinaryTreeIterative bt = BinaryTreeIterative(first_value);
	// WHEN
	int depth = bt.MaxDepth();
	// THEN
	ASSERT_EQ(depth, 1);
}

TEST(BinaryTree, ShouldGetDepthOfTwoWhenOneLeafUnderRootOnLeft) {
	// GIVEN
	int root_value = 2;
	int second_value = 1;
	BinaryTreeIterative bt = BinaryTreeIterative(root_value);
	bt.AddNode(second_value);
	// WHEN
	int depth = bt.MaxDepth();
	// THEN
	ASSERT_EQ(depth, 2);
}

TEST(BinaryTree, ShouldGetDepthOfTwoWhenOneLeafUnderRootOnRight) {
	// GIVEN
	int root_value = 2;
	int second_value = 3;
	BinaryTreeIterative bt = BinaryTreeIterative(root_value);
	bt.AddNode(second_value);
	// WHEN
	int depth = bt.MaxDepth();
	// THEN
	ASSERT_EQ(depth, 2);
}

TEST(BinaryTree, ShouldGetDepthOfThreeWhenTwoLeafsUnderRootOnLefLeft) {
	// GIVEN
	int root_value = 3;
	int second_value = 2;
	int third_value = 1;
	BinaryTreeIterative bt = BinaryTreeIterative(root_value);
	bt.AddNode(second_value);
	bt.AddNode(third_value);
	// WHEN
	int depth = bt.MaxDepth();
	// THEN
	ASSERT_EQ(depth, 3);
}

void ShouldCountNumberOfGivenValueBigNumbers(BinaryTree& bt) {
	
	int first_value = 7500;
	int second_value = 2500;

	for (int i = 0;i < 2499;i++)
		bt.AddNode(first_value-1);

	for (int i = 0;i < 2499;i++)
		bt.AddNode(second_value+i);


	int number = bt.MaxDepth();
	cout << number;
	ASSERT_EQ(number, 2500);
}


TEST(BinaryTree, ShouldCountNumberOfGivenValueRecursivelyWithBigNumbers) {
	ShouldCountNumberOfGivenValueBigNumbers(BinaryTreeRecursive(5000));
}
TEST(BinaryTree, ShouldCountNumberOfGivenValueIterativelyWithBigNumbers) {
	ShouldCountNumberOfGivenValueBigNumbers(BinaryTreeIterative(5000));
}


/*TEST(BinaryTree, ShouldRemoveNodeFromTreeWhenDeleted) {
	//GIVEN
	int root_value = 3;
	int first_value = 2;
	int second_value = 1;
	int third_value = 4;
	int delete_value = 5;

	BinaryTree bt = BinaryTree(root_value);
	bt.AddNode(first_value);
	bt.AddNode(second_value);
	bt.AddNode(third_value);
	bt.AddNode(delete_value);
	bt.AddNode(delete_value);
	bt.AddNode(delete_value);
	ASSERT_EQ(bt.root->right->right->left->left->value, delete_value);


	//WHEN-THEN
	for (BinaryTreeNode& n : bt.getNodeByValue(delete_value))
		bt.delete(n);

	//THEN
	ASSERT_EQ(bt.root->right-right, NULL);
}
*/