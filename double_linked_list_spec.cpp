#include "pch.h"

using namespace std;


TEST(DoubleLinkedList, ShouldAppendElementsToTheEndOfTheListWhenAddedToTail) {
	// GIVEN
	DoubleLinkedList n;

	// WHEN
	for (int i = 1;i < 6;i++) {
		n.addtail(i);
	}


	// THEN
	ASSERT_EQ(n.listAllElements(), "12345");
}

TEST(DoubleLinkedList, ShouldAddElementsToTheBeginingOfTheListWhenAddedToHead) {
	// GIVEN
	DoubleLinkedList n;


	// WHEN
	for (int i = 1;i < 6;i++) {
		n.addhead(i);
	}

	// THEN
	ASSERT_EQ(n.listAllElements(), "54321");
}

TEST(DoubleLinkedList, ShouldRemoveElementFromTheBeginingOfList) {
	DoubleLinkedList n;

	for (int i = 1;i < 6;i++) {
		n.addtail(i);
	}

	cout << n.remove(0)<<'\n';

	ASSERT_EQ(n.listAllElements(), "2345");
}


TEST(DoubleLinkedList, ShouldRemoveElementFromTheEndOfList) {
	DoubleLinkedList n;

	for (int i = 1;i < 6;i++) {
		n.addtail(i);
	}

	cout<<n.remove(4)<<'\n';

	ASSERT_EQ(n.listAllElements(), "1234");
}

TEST(DoubleLinkedList, ShouldRemoveElementFromTheMiddleOfList) {
	DoubleLinkedList n;

	for (int i = 1;i < 6;i++) {
		n.addtail(i);
	}

	cout << n.remove(3)<< '\n';

	ASSERT_EQ(n.listAllElements(), "1235");
}

TEST(DoubleLinkedList, ShouldDonothingWhenGivenWrongIndex) {
	DoubleLinkedList n;

	for (int i = 1;i < 6;i++) {
		n.addtail(i);
	}

	cout << n.remove(99) << '\n';

	ASSERT_EQ(n.listAllElements(), "12345");
}

TEST(DoubleLinkedList, ShouldFindFirstIndexWithGivenValue) {
	DoubleLinkedList n;

	for (int i = 1;i < 6;i++) {
		n.addtail(i);
	}

	for (int i = 1;i < 6;i++) {
		n.addtail(i);
	}

	//cout<<n.listAllElements()<<'\n';

	ASSERT_EQ(n.find(1), 0);

}

TEST(DoubleLinkedList, ShouldFindSecondIndexWithGivenValue) {
	DoubleLinkedList n;

	for (int i = 1;i < 6;i++) {
		n.addtail(i);
	}
	for (int i = 1;i < 6;i++) {
		n.addtail(i);
	}

	ASSERT_EQ(n.find(1, 0), 5);
}

TEST(DoubleLinkedList, ErrorFromFind) {
	DoubleLinkedList n;

	for (int i = 1;i < 6;i++) {
		n.addtail(i);
	}

	ASSERT_EQ(n.find(99), -99999999);
}

TEST(DoubleLinkedList, ShouldGiveSizeOfEmptyList) {
	DoubleLinkedList n;


	ASSERT_EQ(n.size(), 0);
}

TEST(DoubleLinkedList, ShouldGiveSizeOfList) {
	DoubleLinkedList n;

	for (int i = 1;i < 6;i++) {
		n.addtail(i);
	}

	ASSERT_EQ(n.size(), 5);
}

TEST(DoubleLinkedList, ShouldGetElement) {
	DoubleLinkedList n;
	for (int i = 1;i < 6;i++) {
		n.addtail(i);
	}

	ASSERT_EQ(n.getelement(1), 2);
}


TEST(DoubleLinkedList, ShouldAddTwoLists) {
	DoubleLinkedList a;
	DoubleLinkedList b;
	DoubleLinkedList c;
	for (int i = 1;i < 6;i++) {
		a.addtail(i);
	}
	for (int i = 1;i < 6;i++) {
		b.addtail(i);
	}

	c = a + b;

	ASSERT_EQ(c.listAllElements(), "1234512345");
}



TEST(DoubleLinkedList, ShouldAssignListTo) {
	DoubleLinkedList a;
	DoubleLinkedList b;


	a.addtail(1);
	a.addtail(2);
	b.addtail(3);
	b.addtail(4);

	a = b;

	ASSERT_EQ(a.listAllElements(), "34");
}

TEST(DoubleLinkedList, ShouldBeEmptyWhenLastElementRemoved) {
	DoubleLinkedList n;

	n.addhead(1);

	n.remove(0);

	ASSERT_EQ(n.listAllElements(), "");
}

TEST(DoubleLinkedList, ShouldBeEmptyWhenCleared) {
	DoubleLinkedList n;

	for (int i = 1;i < 6;i++) {
		n.addtail(i);
	}

	n.clear();

	ASSERT_EQ(n.listAllElements(), "");
}

TEST(DoubleLinkedList, ShouldIterateThrougList) {
	DoubleLinkedList n;
	string result = "";

	for (int i = 1;i < 6;i++) {
		n.addtail(i);
	}

	for (DoubleLinkedListElement& x : n) {
		result += to_string(x.value);
	}
	cout << n.listAllElements();
	ASSERT_EQ(result, "12345");
}
