#pragma once
#include "pch.h"

using namespace std;


class BinaryTreeNode {
public:
	int value;
	BinaryTreeNode* left = NULL;
	BinaryTreeNode* right = NULL;

	BinaryTreeNode(int value) : value(value) {};
	bool isLastLeaf();
};

class BinaryTree
{
public:
	BinaryTreeNode* root = NULL;

	BinaryTree(int value);
	void AddNode(int value);
	virtual int CountValue(int value);
	virtual int MaxDepth();
};

class BinaryTreeRecursive : public BinaryTree {
private:
	int CountValueRec(int value, BinaryTreeNode** leaf = NULL, int if_first = 1, int counter = 0);
	int MaxDepthWithParameter(BinaryTreeNode* node);
public:
	BinaryTreeRecursive(int value);
	int CountValue(int value);
	int MaxDepth();
};

class BinaryTreeIterative : public BinaryTree {
public:
	BinaryTreeIterative(int value);
	int CountValue(int value);
	int MaxDepth();
};
